﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APIS
{
    class FileManager
    {
        public Image<Bgr, Byte> image;
        public List<Image<Bgr, Byte>> images;

        public FileManager()
        {
            images = new List<Image<Bgr, byte>>();
        }

        public void SaveImage(Image<Bgr, Byte> img)
        {
            //ImageFrame.Save(@"E:\capture");
            //Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Image Files (*.tif; *.dcm; *.jpg; *.jpeg; *.bmp)|*.tif; *.dcm; *.jpg; *.jpeg; *.bmp";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    // Code to write the stream goes here.
                    img.Save(saveFileDialog1.FileName);
                    //myStream.Close();
                }
            }
        }

        public void SaveImages()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Image Files (*.tif; *.dcm; *.jpg; *.jpeg; *.bmp)|*.tif; *.dcm; *.jpg; *.jpeg; *.bmp";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                int i = 1;
                foreach (var img in images)
                {
                    img.Save(saveFileDialog1.FileName.Split('.').First() + i.ToString() + '.' + saveFileDialog1.FileName.Split('.').Last());
                    ++i;
                }
            }
            images.Clear();
        }

        public void LoadImages()
        {
            images.Clear();
            var fbd = new FolderBrowserDialog();
            //ofd.Title = "Wybierz katalog";
            //ofd.Multiselect = true;
            //ofd.Filter = "JPG|*.jpg|JPEG|*.jpeg|PNG|*.png|BMP|*.bmp|TIFF|*.tif";
            DialogResult dr = fbd.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                string[] files = Directory.GetFiles(fbd.SelectedPath);

                LoadGroup(files);
            }
        }

        public void LoadImage()
        {
            images.Clear();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Wybierz obrazki";
            //ofd.Multiselect = true;
            ofd.Filter = "All files (*.*)|*.*|JPG|*.jpg|JPEG|*.jpeg|PNG|*.png|BMP|*.bmp|TIFF|*.tif";
            DialogResult dr = ofd.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                string[] files = ofd.FileNames;

                LoadSingle(files);
            }
        }

        private void LoadGroup(string[] files)
        {
            foreach (string img in files)
            {
                var image = new Image<Bgr, byte>(img);
                images.Add(image);
            }
        }

        private void LoadSingle(string[] files)
        {
            foreach (string img in files)
            {
                Bitmap bmpImage = new Bitmap(img);
                var a = new Emgu.CV.Image<Bgr, Byte>(bmpImage);
                image = a.Clone();
            }
        }
    }
}
