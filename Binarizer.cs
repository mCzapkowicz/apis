﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIS
{
    class Binarizer
    {


        public Image<Gray, byte> BinarizeWithOpening(Image<Bgr, byte> img)
        {
            var skin = BinarizeHsv1(img);

            return skin;
        }

        public Image<Gray, byte> BinarizeHsv1(Image<Bgr, byte> ImageFrame)
        {
            var skin = BinarizeWithHsvFilter(ImageFrame);
            Size a = new Size(15, 15);
            Mat rect_2 = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(3, 3), new Point(-1, -1));
            Mat rect_6 = CvInvoke.GetStructuringElement(ElementShape.Ellipse, new Size(4, 4), new Point(-1, -1));
            CvInvoke.Erode(skin, skin, rect_2, new Point(-1, -1), 3, BorderType.Reflect, default(MCvScalar));
            //CvInvoke.Dilate(skin, skin, rect_2, new Point(-1, -1), 3, BorderType.Reflect, default(MCvScalar));
            //CvInvoke.Erode(skin, skin, rect_2, new Point(-1, -1), 2, BorderType.Reflect, default(MCvScalar));

            CvInvoke.Dilate(skin, skin, rect_2, new Point(-1, -1), 7, BorderType.Reflect, default(MCvScalar));


            //CvInvoke.Dilate(skin, skin, rect_2, new Point(-1, -1), 2, BorderType.Reflect, default(MCvScalar));

            CvInvoke.Blur(skin, skin, a, new Point(-1, -1));
            CvInvoke.Threshold(skin, skin, 50, 255, ThresholdType.Binary);

            return skin;
        }

        public Image<Gray, byte> BinarizeRgb1(Image<Bgr, byte> ImageFrame)
        {
            //var i = BinarizeInputImage(img);


            //CvInvoke.cvDilate(skin, skin, rect_6, 2);

            //i.Erode(5000);
            //double cannyThreshold = 180.0;
            //double circleAccumulatorThreshold = 120;
            //CircleF[] circles = CvInvoke.HoughCircles(i, HoughType.Gradient, 2.0, 20.0, cannyThreshold, circleAccumulatorThreshold, 5);

            //i = i.Erode(6);
            //i = i.Dilate(10).Erode(6);
            Size a = new Size(15, 15);
            //CvInvoke.Blur(i, i, a, new Point(-1,-1));
            //CvInvoke.Threshold(i, i, 200, 255, ThresholdType.Binary);
            //i = i.Erode(2);
            //i = i.Dilate(3);
            // var skin = BinarizeWithHsvFilter(img);
            var skin = BinarizeWithRgbDiff(ImageFrame);
            //var skin2 = BinarizeWithRgbDiff(ImageFrame);
            //skin = MultiplyBinary(skin, skin2);
            //var skin = BinarizeWithYCrCbColor(img);


            
            Mat rect_2 = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(3, 3), new Point(-1, -1));
            Mat rect_6 = CvInvoke.GetStructuringElement(ElementShape.Ellipse, new Size(5, 5), new Point(-1, -1));
            Mat r3 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(3, 3), new Point(-1, -1));
            //CvInvoke.Erode(skin, skin, rect_2, new Point(-1, -1), 3, BorderType.Reflect, default(MCvScalar));
            //CvInvoke.Dilate(skin, skin, rect_2, new Point(-1, -1), 3, BorderType.Reflect, default(MCvScalar));
            //CvInvoke.Erode(skin, skin, r3, new Point(-1, -1), 10, BorderType.Reflect, default(MCvScalar));
            //CvInvoke.Dilate(skin, skin, rect_2, new Point(-1, -1), 5, BorderType.Reflect, default(MCvScalar));

            //CvInvoke.Dilate(skin, skin, rect_2, new Point(-1, -1), 2, BorderType.Reflect, default(MCvScalar));
            //CvInvoke.Blur(skin, skin, a, new Point(-1, -1));
            //CvInvoke.Threshold(skin, skin, 150, 255, ThresholdType.Binary);

            //CvInvoke.Erode(skin, skin, rect_6, new Point(-1, -1), 2, BorderType.Reflect, default(MCvScalar));
            

            CvInvoke.Blur(skin, skin, a, new Point(-1, -1));
            CvInvoke.Threshold(skin, skin, 50, 255, ThresholdType.Binary);

            return skin;
        }

        public Image<Gray, byte> BinarizeWithHsvFilter(Image<Bgr, byte> ImageFrame)
        {
            //Image<Bgr, Byte> imgBrg = new Image<Bgr, Byte>(size);
            //imgBrg = capture.QueryFrame();

            Image<Hsv, Byte> imgHsv = ImageFrame.Convert<Hsv, Byte>();
            Image<Gray, Byte> grayOut = new Image<Gray, Byte>(ImageFrame.Width, ImageFrame.Height);
            for (int x = 0; x < imgHsv.Width; x++)
            {
                for (int y = 0; y < imgHsv.Height; y++)
                {
                    byte a = 0;
                    byte b = 255;
                    Hsv color = imgHsv[y, x];
                    Bgr color2 = ImageFrame[y, x];
                    //if (color.Hue < 25 && color.Satuation > 25 && color.Satuation < 100)
                    //if (color.Satuation > 90)//&& !((color.Hue > 20) && (color.Hue < 180)))
                    if(color.Hue < 30 && color.Satuation > 60)
                        grayOut.Data[y, x, 0] = b;
                    else
                        grayOut.Data[y, x, 0] = a;
                    /*
                    if (IsRgbDiffBiggerThan2(15, color))
                        grayOut.Data[y, x, 0] = b;
                    else
                        grayOut.Data[y, x, 0] = a;
                    */
                }
            }
            //Bgr color = ImageFrame

            return grayOut;//ImageFrame.InRange(new Bgr(20, 40, 100), new Bgr(150, 180, 230));

        }

        public bool IsRgbDiffBiggerThan2(int difference, Bgr color)
        {
            if (((color.Red - color.Green) > difference))// && 
                //((color.Green - color.Blue) > difference))
                return true;
            else return false;
        }

        public Image<Gray, byte> BinarizeWithYCrCbColor(Image<Bgr, byte> img)
        {
            Ycc YCrCb_min = new Ycc(0, 131, 80);
            Ycc YCrCb_max = new Ycc(255, 185, 135);

            Image<Ycc, Byte> currentYCrCbFrame = img.Convert<Ycc, Byte>();
            Image<Gray, byte> skin = new Image<Gray, byte>(img.Width, img.Height);
            skin = currentYCrCbFrame.InRange((Ycc)YCrCb_min, (Ycc)YCrCb_max);
            return skin;
        }

        public Image<Gray, Byte> MultiplyBinary(Image<Gray, Byte> Bin1, Image<Gray, Byte> Bin2)
        {
            Image<Gray, Byte> newRgb = new Image<Gray, Byte>(Bin1.Width, Bin1.Height);
            for (int x = 0; x < Bin1.Width; x++)
            {
                for (int y = 0; y < Bin1.Height; y++)
                {
                    byte a = 0;
                    byte b = 255;
                    byte color1 = Bin1.Data[y, x, 0];
                    byte color2 = Bin2.Data[y, x, 0];

                    if(color1 == b && color2 == b)
                    {
                        newRgb.Data[y, x, 0] = b;
                    }
                    else
                    {
                        newRgb.Data[y, x, 0] = a;
                    }
                }
            }

            return newRgb;
        }

        public Image<Gray, Byte> BinarizeWithRgbDiff2(Image<Bgr, Byte> ImageFrame)
        {
            Image<Gray, Byte> grayOut = new Image<Gray, Byte>(ImageFrame.Width, ImageFrame.Height);
            for (int x = 0; x < ImageFrame.Width; x++)
            {
                for (int y = 0; y < ImageFrame.Height; y++)
                {
                    byte a = 0;
                    byte b = 255;
                    Bgr color = ImageFrame[y, x];
                    if (IsRgbDiffOk(color))
                        grayOut.Data[y, x, 0] = b;
                    else
                        grayOut.Data[y, x, 0] = a;
                }
            }
            //Bgr color = ImageFrame

            return grayOut;//ImageFrame.InRange(new Bgr(20, 40, 100), new Bgr(150, 180, 230));
        }

        public Image<Gray, Byte> BinarizeWithRgbDiff(Image<Bgr, Byte> ImageFrame)
        {
            Image<Gray, Byte> grayOut = new Image<Gray, Byte>(ImageFrame.Width, ImageFrame.Height);
            for (int x = 0; x < ImageFrame.Width; x++)
            {
                for (int y = 0; y < ImageFrame.Height; y++)
                {
                    byte a = 0;
                    byte b = 255;
                    Bgr color = ImageFrame[y, x];
                    if (IsRgbDiffBiggerThan(35, color))
                        grayOut.Data[y, x, 0] = b;
                    else
                        grayOut.Data[y, x, 0] = a;
                }
            }
            //Bgr color = ImageFrame

            return grayOut;//ImageFrame.InRange(new Bgr(20, 40, 100), new Bgr(150, 180, 230));
        }

        public bool IsRgbDiffBiggerThan(int difference, Bgr color)
        {
            var c1 = color.Red - color.Blue;
            var c2 = color.Red - color.Green;
            var c3 = color.Blue - color.Green;

            var c = c1 > c2 ? c1 : c2;
            c = c3 > c ? c3 : c;
            if (c > difference)
                return true;
            return false;
        }

        public bool IsRgbDiffOk(Bgr color)
        {
            var rb = color.Red - color.Blue;
            var rg = color.Red - color.Green;
            var gb = color.Green - color.Blue;


            if(rg > 0 )
            {
                if(gb > 0)
                {
                    if(rg/gb > 0.7)
                    {
                        return true;
                    }
                } 
            }
            


            return false;
        }
    }
}
