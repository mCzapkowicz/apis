﻿

namespace APIS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imageBox2 = new Emgu.CV.UI.ImageBox();
            this.captureCamera = new System.Windows.Forms.Button();
            this.imageBox3 = new Emgu.CV.UI.ImageBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.imageBox4 = new Emgu.CV.UI.ImageBox();
            this.trackObjects = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.imageBox5 = new Emgu.CV.UI.ImageBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startSaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopSaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSingleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageBox6 = new Emgu.CV.UI.ImageBox();
            this.ClusterNumber = new System.Windows.Forms.Label();
            this.imageBox7 = new Emgu.CV.UI.ImageBox();
            this.solidity = new System.Windows.Forms.Label();
            this.area = new System.Windows.Forms.Label();
            this.hull_area = new System.Windows.Forms.Label();
            this.gestureNumber = new System.Windows.Forms.Label();
            this.roundness = new System.Windows.Forms.Label();
            this.Rs = new System.Windows.Forms.Label();
            this.boxarea = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox3)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox5)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // imageBox1
            // 
            this.imageBox1.Location = new System.Drawing.Point(12, 29);
            this.imageBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(400, 301);
            this.imageBox1.TabIndex = 2;
            this.imageBox1.TabStop = false;
            this.imageBox1.Click += new System.EventHandler(this.imageBox1_Click);
            this.imageBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseDown);
            this.imageBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseMove);
            this.imageBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageBox1_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(546, 20);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(408, 310);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // imageBox2
            // 
            this.imageBox2.Location = new System.Drawing.Point(557, 28);
            this.imageBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.imageBox2.Name = "imageBox2";
            this.imageBox2.Size = new System.Drawing.Size(400, 301);
            this.imageBox2.TabIndex = 2;
            this.imageBox2.TabStop = false;
            // 
            // captureCamera
            // 
            this.captureCamera.Location = new System.Drawing.Point(440, 28);
            this.captureCamera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.captureCamera.Name = "captureCamera";
            this.captureCamera.Size = new System.Drawing.Size(81, 22);
            this.captureCamera.TabIndex = 8;
            this.captureCamera.Text = "Capture camera";
            this.captureCamera.UseVisualStyleBackColor = true;
            this.captureCamera.Click += new System.EventHandler(this.captureCamera_Click);
            // 
            // imageBox3
            // 
            this.imageBox3.Location = new System.Drawing.Point(84, 341);
            this.imageBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.imageBox3.Name = "imageBox3";
            this.imageBox3.Size = new System.Drawing.Size(266, 201);
            this.imageBox3.TabIndex = 2;
            this.imageBox3.TabStop = false;
            this.imageBox3.Click += new System.EventHandler(this.imageBox3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "label2";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(440, 87);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(81, 88);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Object coords";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "label3";
            // 
            // imageBox4
            // 
            this.imageBox4.Location = new System.Drawing.Point(356, 342);
            this.imageBox4.Name = "imageBox4";
            this.imageBox4.Size = new System.Drawing.Size(266, 200);
            this.imageBox4.TabIndex = 2;
            this.imageBox4.TabStop = false;
            this.imageBox4.Click += new System.EventHandler(this.imageBox4_Click);
            // 
            // trackObjects
            // 
            this.trackObjects.Location = new System.Drawing.Point(440, 182);
            this.trackObjects.Name = "trackObjects";
            this.trackObjects.Size = new System.Drawing.Size(81, 23);
            this.trackObjects.TabIndex = 12;
            this.trackObjects.Text = "Track";
            this.trackObjects.UseVisualStyleBackColor = true;
            this.trackObjects.Click += new System.EventHandler(this.trackObjects_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(449, 211);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 21);
            this.button1.TabIndex = 15;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // imageBox5
            // 
            this.imageBox5.Location = new System.Drawing.Point(84, 549);
            this.imageBox5.Name = "imageBox5";
            this.imageBox5.Size = new System.Drawing.Size(266, 200);
            this.imageBox5.TabIndex = 16;
            this.imageBox5.TabStop = false;
            this.imageBox5.Click += new System.EventHandler(this.imageBox5_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(969, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveImageToolStripMenuItem,
            this.saveImageGroupToolStripMenuItem,
            this.loadImageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.imageToolStripMenuItem.Text = "Image";
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.saveImageToolStripMenuItem.Text = "Save image";
            this.saveImageToolStripMenuItem.Click += new System.EventHandler(this.saveImageToolStripMenuItem_Click);
            // 
            // saveImageGroupToolStripMenuItem
            // 
            this.saveImageGroupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startSaveToolStripMenuItem,
            this.stopSaveToolStripMenuItem});
            this.saveImageGroupToolStripMenuItem.Name = "saveImageGroupToolStripMenuItem";
            this.saveImageGroupToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.saveImageGroupToolStripMenuItem.Text = "Save image group";
            // 
            // startSaveToolStripMenuItem
            // 
            this.startSaveToolStripMenuItem.Name = "startSaveToolStripMenuItem";
            this.startSaveToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.startSaveToolStripMenuItem.Text = "Start save";
            this.startSaveToolStripMenuItem.Click += new System.EventHandler(this.startSaveToolStripMenuItem_Click);
            // 
            // stopSaveToolStripMenuItem
            // 
            this.stopSaveToolStripMenuItem.Name = "stopSaveToolStripMenuItem";
            this.stopSaveToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.stopSaveToolStripMenuItem.Text = "Stop save";
            this.stopSaveToolStripMenuItem.Click += new System.EventHandler(this.stopSaveToolStripMenuItem_Click);
            // 
            // loadImageToolStripMenuItem
            // 
            this.loadImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadSingleToolStripMenuItem,
            this.loadGroupToolStripMenuItem});
            this.loadImageToolStripMenuItem.Name = "loadImageToolStripMenuItem";
            this.loadImageToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.loadImageToolStripMenuItem.Text = "Load image";
            this.loadImageToolStripMenuItem.Click += new System.EventHandler(this.loadImageToolStripMenuItem_Click);
            // 
            // loadSingleToolStripMenuItem
            // 
            this.loadSingleToolStripMenuItem.Name = "loadSingleToolStripMenuItem";
            this.loadSingleToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.loadSingleToolStripMenuItem.Text = "Load single...";
            this.loadSingleToolStripMenuItem.Click += new System.EventHandler(this.loadSingleToolStripMenuItem_Click);
            // 
            // loadGroupToolStripMenuItem
            // 
            this.loadGroupToolStripMenuItem.Name = "loadGroupToolStripMenuItem";
            this.loadGroupToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.loadGroupToolStripMenuItem.Text = "Load group...";
            this.loadGroupToolStripMenuItem.Click += new System.EventHandler(this.loadGroupToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // imageBox6
            // 
            this.imageBox6.Location = new System.Drawing.Point(628, 342);
            this.imageBox6.Name = "imageBox6";
            this.imageBox6.Size = new System.Drawing.Size(266, 200);
            this.imageBox6.TabIndex = 18;
            this.imageBox6.TabStop = false;
            this.imageBox6.Click += new System.EventHandler(this.imageBox6_Click);
            // 
            // ClusterNumber
            // 
            this.ClusterNumber.AutoSize = true;
            this.ClusterNumber.Location = new System.Drawing.Point(805, 717);
            this.ClusterNumber.Name = "ClusterNumber";
            this.ClusterNumber.Size = new System.Drawing.Size(35, 13);
            this.ClusterNumber.TabIndex = 19;
            this.ClusterNumber.Text = "label5";
            // 
            // imageBox7
            // 
            this.imageBox7.Location = new System.Drawing.Point(356, 548);
            this.imageBox7.Name = "imageBox7";
            this.imageBox7.Size = new System.Drawing.Size(266, 200);
            this.imageBox7.TabIndex = 20;
            this.imageBox7.TabStop = false;
            this.imageBox7.Click += new System.EventHandler(this.imageBox7_Click);
            // 
            // solidity
            // 
            this.solidity.AutoSize = true;
            this.solidity.Location = new System.Drawing.Point(805, 600);
            this.solidity.Name = "solidity";
            this.solidity.Size = new System.Drawing.Size(38, 13);
            this.solidity.TabIndex = 22;
            this.solidity.Text = "solidity";
            this.solidity.Click += new System.EventHandler(this.label5_Click);
            // 
            // area
            // 
            this.area.AutoSize = true;
            this.area.Location = new System.Drawing.Point(805, 578);
            this.area.Name = "area";
            this.area.Size = new System.Drawing.Size(28, 13);
            this.area.TabIndex = 23;
            this.area.Text = "area";
            this.area.Click += new System.EventHandler(this.label5_Click_1);
            // 
            // hull_area
            // 
            this.hull_area.AutoSize = true;
            this.hull_area.Location = new System.Drawing.Point(805, 558);
            this.hull_area.Name = "hull_area";
            this.hull_area.Size = new System.Drawing.Size(44, 13);
            this.hull_area.TabIndex = 24;
            this.hull_area.Text = "hullarea";
            // 
            // gestureNumber
            // 
            this.gestureNumber.AutoSize = true;
            this.gestureNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gestureNumber.Location = new System.Drawing.Point(628, 558);
            this.gestureNumber.Name = "gestureNumber";
            this.gestureNumber.Size = new System.Drawing.Size(83, 39);
            this.gestureNumber.TabIndex = 25;
            this.gestureNumber.Text = "brak";
            // 
            // roundness
            // 
            this.roundness.AutoSize = true;
            this.roundness.Location = new System.Drawing.Point(805, 622);
            this.roundness.Name = "roundness";
            this.roundness.Size = new System.Drawing.Size(56, 13);
            this.roundness.TabIndex = 26;
            this.roundness.Text = "roundness";
            // 
            // Rs
            // 
            this.Rs.AutoSize = true;
            this.Rs.Location = new System.Drawing.Point(805, 645);
            this.Rs.Name = "Rs";
            this.Rs.Size = new System.Drawing.Size(20, 13);
            this.Rs.TabIndex = 27;
            this.Rs.Text = "Rs";
            // 
            // boxarea
            // 
            this.boxarea.AutoSize = true;
            this.boxarea.Location = new System.Drawing.Point(805, 669);
            this.boxarea.Name = "boxarea";
            this.boxarea.Size = new System.Drawing.Size(40, 13);
            this.boxarea.TabIndex = 28;
            this.boxarea.Text = "bbarea";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(738, 558);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Hull area";
            this.label5.Click += new System.EventHandler(this.label5_Click_2);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(738, 578);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Obj area";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(738, 600);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Solidity";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(738, 622);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Roundness";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(738, 645);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Rs";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(738, 669);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Bbox area";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(738, 717);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "Clusters";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 807);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.boxarea);
            this.Controls.Add(this.Rs);
            this.Controls.Add(this.roundness);
            this.Controls.Add(this.gestureNumber);
            this.Controls.Add(this.hull_area);
            this.Controls.Add(this.area);
            this.Controls.Add(this.solidity);
            this.Controls.Add(this.imageBox7);
            this.Controls.Add(this.ClusterNumber);
            this.Controls.Add(this.imageBox6);
            this.Controls.Add(this.imageBox5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.trackObjects);
            this.Controls.Add(this.imageBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.imageBox3);
            this.Controls.Add(this.captureCamera);
            this.Controls.Add(this.imageBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.imageBox1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox3)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox5)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Emgu.CV.UI.ImageBox imageBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Emgu.CV.UI.ImageBox imageBox2;
        private System.Windows.Forms.Button captureCamera;
        private Emgu.CV.UI.ImageBox imageBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private Emgu.CV.UI.ImageBox imageBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button trackObjects;
        private System.Windows.Forms.Button button1;
        private Emgu.CV.UI.ImageBox imageBox5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveImageGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startSaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopSaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSingleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadGroupToolStripMenuItem;
        private Emgu.CV.UI.ImageBox imageBox6;
        private System.Windows.Forms.Label ClusterNumber;
        private Emgu.CV.UI.ImageBox imageBox7;
        private System.Windows.Forms.Label solidity;
        private System.Windows.Forms.Label area;
        private System.Windows.Forms.Label hull_area;
        private System.Windows.Forms.Label gestureNumber;
        private System.Windows.Forms.Label roundness;
        private System.Windows.Forms.Label Rs;
        private System.Windows.Forms.Label boxarea;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

