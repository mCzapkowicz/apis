﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIS
{
    public class ObjectHandler
    {
        public Image<Gray, byte> mainImage;

        public ObjectHandler()
        {

        }

        

        public VectorOfVectorOfPoint FindContour(UMat img, Mat hierarchy = null, Emgu.CV.CvEnum.RetrType type = Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod method = Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple)
        {
            var contours = new VectorOfVectorOfPoint();
            
            CvInvoke.FindContours(img, contours, hierarchy, type, method);
            return contours;
        }
        //public void SetImage(Image<Bgr,byte>)
        /*

        public void CalculateConvexHull(Image<Gray, byte> img)
        {
            //List<Point> contours = new List<Point>();
            //List<Point> hierarchy = new List<Point>();
            //Mat contours;
            Emgu.CV.Util.VectorOfVectorOfPoint vec = new Emgu.CV.Util.VectorOfVectorOfPoint();
            //CvInvoke.FindContours(img, contours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxSimple);
            //img.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage
            vec = FindContour(img.ToUMat());
            Image<Bgr, byte> temp = ImageFrame.Clone();
            //CvInvoke.DrawContours(temp, vec, -1, new MCvScalar(255, 0, 0),5);
            double size = 0;
            int idx = 0;
            for (int i = 0; i < vec.Size; i++)
            {
                double size1 = CvInvoke.ContourArea(vec[i]);
                if (size1 > size)
                {
                    size = size1;
                    idx = i;
                }
            }
            var minarearect = CvInvoke.MinAreaRect(vec[idx]);
            //CvInvoke.DrawContours(temp, minarearect, -1, new MCvScalar(255, 0, 0), 5);
            VectorOfVectorOfPoint vec2 = new Emgu.CV.Util.VectorOfVectorOfPoint(vec.Size);
            //var i = vec[2];
            //var j = i[1];
            for (int i = 0; i < vec.Size; i++)
            {
                CvInvoke.ConvexHull(vec[i], vec2[i], false);
            }

            //CvInvoke.DrawContours(temp, vec2, -1, new MCvScalar(0, 255, 0), 5);
            for (int i = 0; i < vec2.Size; i++)
            {
                CvInvoke.DrawContours(temp, vec, i, new MCvScalar(255, 0, 0), 5);
                CvInvoke.DrawContours(temp, vec2, i, new MCvScalar(0, 255, 0), 5);
                for (int j = 0; j < vec2[i].Size; j++)
                {
                    temp.Draw(new Rectangle(vec2[i][j].X, vec2[i][j].Y, 5, 5), new Bgr(0, 0, 255), 3);
                }
                //temp.Draw(new Rectangle((int)vec2[i].X, (int)vec2[i].Y, 0, 0), new Bgr(0, 0, 255), 3);
            }
            //img.Fi
            //CvInvoke.ConvexHull(vec.ToArrayOfArray());
            //imageBox6.Image = temp;
            imageBox6.Image = temp.Resize(imageBox6.Width, imageBox6.Height, Inter.Linear);
        }

        public VectorOfVectorOfPoint FindContour(UMat img, Mat hierarchy = null, Emgu.CV.CvEnum.RetrType type = Emgu.CV.CvEnum.RetrType.List, Emgu.CV.CvEnum.ChainApproxMethod method = Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple)
        {
            var contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(img, contours, hierarchy, type, method);
            return contours;
        }



        public Image<Bgr, byte> DrawHelperLines(Image<Gray, Byte> Binary)
        {
            List<Point> objectPoints = new List<Point>();
            GetObjectCoords(Binary, objectPoints);
            Image<Bgr, byte> imageWithLines = ImageFrame.Clone();
            if (objectPoints[0] == objectPoints[1]) return imageWithLines;

            var color = new MCvScalar(0, 255, 0, 0);
            int labelIterator = 0;
            foreach (var point in objectPoints)
            {
                CvInvoke.Circle(imageWithLines, point, 30, color);
                labels[labelIterator].Text = "x: " + point.X + ", y: " + point.Y;
                labelIterator++;
            }

            LineSegment2D line1 = new LineSegment2D(objectPoints[0], objectPoints[1]);
            LineSegment2D line2 = new LineSegment2D(objectPoints[2], objectPoints[3]);
            imageWithLines.Draw(line1, new Bgr(255, 0, 0), 2);
            imageWithLines.Draw(line2, new Bgr(255, 0, 0), 2);

            Point inters = GetIntersection(line1, line2);
            CvInvoke.Circle(imageWithLines, inters, 30, new MCvScalar(0, 0, 0, 255));
            if (movementPoints.Any() && movementFlag)
            {
                var dist = PointDistance(inters, movementPoints.Last());
                if (dist > 20 && dist < 300)
                {
                    movementPoints.Add(inters);
                }
            }
            else if (movementFlag)
            {
                movementPoints.Add(inters);

            }
            else
                movementPoints.Clear();

            if (movementPoints.Any())
            {
                Point last = movementPoints[0];
                foreach (var point in movementPoints)
                {
                    //CvInvoke.Circle(ImageFrame, point, 2, new MCvScalar(0, 0, 255, 0));
                    CvInvoke.Line(imageWithLines, last, point, new MCvScalar(0, 0, 255, 0), 2);
                    last = point;
                }
            }

            return imageWithLines;
        }
        
        */
        public double PointDistance(Point A, Point B)
        {

            return Math.Sqrt(Math.Pow((A.X - B.X), 2) + Math.Pow((A.Y - B.Y), 2));
        }

        public Point GetIntersection(LineSegment2D line1, LineSegment2D line2)
        {

            double a1 = (line1.P1.Y - line1.P2.Y) / (double)(line1.P1.X - line1.P2.X);
            double b1 = line1.P1.Y - a1 * line1.P1.X;

            double a2 = (line2.P1.Y - line2.P2.Y) / (double)(line2.P1.X - line2.P2.X);
            double b2 = line2.P1.Y - a2 * line2.P1.X;

            if (Math.Abs(a1 - a2) < double.Epsilon)
                throw new InvalidOperationException();

            double x = (b2 - b1) / (a1 - a2);
            double y = a1 * x + b1;
            return new Point((int)x, (int)y);
        }



        public List<Point> GetObjectCoords(Image<Gray, Byte> Binary)
        {
            //objectPoints.Clear();
            var objectPoints = new List<Point>();

            Point A = new Point(-1, -1);
            Point B = new Point(-1, -1);
            Point C = new Point(-1, -1);
            Point D = new Point(-1, -1);
            for (int x = 0; x < Binary.Width; x++)
            {
                for (int y = 0; y < Binary.Height; y++)
                {

                    if (A.X == -1 || A.Y == -1)
                    {
                        if (Binary.Data[y, x, 0] > 0)
                        {
                            A.X = x;
                            A.Y = y;
                        }
                    }
                    if (C.X == -1 || C.Y == -1)
                    {
                        if (Binary.Data[y, x, 0] > 0)
                        {
                            C.X = x;
                            C.Y = y;
                        }
                    }
                    if (Binary.Data[y, x, 0] > 0)
                    {
                        if (y > B.Y)
                        {
                            B.X = x;
                            B.Y = y;
                        }

                        if (y < A.Y && y > 0)
                        {
                            A.X = x;
                            A.Y = y;
                        }

                        if (x > D.X)
                        {
                            D.X = x;
                            D.Y = y;
                        }

                        if (x < C.X && x > 0)
                        {
                            C.X = x;
                            C.Y = y;
                        }
                    }
                }
            }

            objectPoints.Add(A);
            objectPoints.Add(B);
            objectPoints.Add(C);
            objectPoints.Add(D);

            return objectPoints;
        }
    }
}
