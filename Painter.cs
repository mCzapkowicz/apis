﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIS
{
    public class Painter
    {

        private List<Point> movementPoints = new List<Point>();
        private Form1 mainWindow;

        public Painter(Form1 form)
        {
            mainWindow = form;
        }
        

        public static void setImageBox(Emgu.CV.UI.ImageBox imageBox, Image<Bgr,byte> image)
        {
            imageBox.Image = image.Resize(imageBox.Width, imageBox.Height, Inter.Linear);
        }
        public static void setImageBox(Emgu.CV.UI.ImageBox imageBox, Image<Gray, byte> image)
        {
            imageBox.Image = image.Resize(imageBox.Width, imageBox.Height, Inter.Linear);
        }

        public static void drawConvexHull(Image<Bgr, byte> temp, VectorOfVectorOfPoint vec, VectorOfVectorOfPoint vec2)
        {
            for (int i = 0; i < vec2.Size; i++)
            {
                CvInvoke.DrawContours(temp, vec, i, new MCvScalar(255, 0, 0), 5);
                CvInvoke.DrawContours(temp, vec2, i, new MCvScalar(0, 255, 0), 5);
                for (int j = 0; j < vec2[i].Size; j++)
                {
                    temp.Draw(new Rectangle(vec2[i][j].X, vec2[i][j].Y, 5, 5), new Bgr(0, 0, 255), 3);
                }
                //temp.Draw(new Rectangle((int)vec2[i].X, (int)vec2[i].Y, 0, 0), new Bgr(0, 0, 255), 3);
            }
        }

        public Image<Bgr, byte> DrawHelperLines(Image<Bgr, byte> ImageFrame, Image<Gray,byte> Binary)
        {

            //using handler = mainWindow.objectHandler;
            var handler = mainWindow.objectHandler;
            

            Image<Bgr, byte> imageWithLines = ImageFrame.Clone();

            List<Point> objectPoints;// = new List<Point>();

            objectPoints = handler.GetObjectCoords(Binary);
            
            if (objectPoints[0] == objectPoints[1]) return imageWithLines;

            LineSegment2D line1 = new LineSegment2D(objectPoints[0], objectPoints[1]);
            LineSegment2D line2 = new LineSegment2D(objectPoints[2], objectPoints[3]);
            Point inters = handler.GetIntersection(line1, line2);




            // draw


            imageWithLines.Draw(line1, new Bgr(255, 0, 0), 2);
            imageWithLines.Draw(line2, new Bgr(255, 0, 0), 2);



            var color = new MCvScalar(0, 255, 0, 0);
            int labelIterator = 0;
            foreach (var point in objectPoints)
            {
                CvInvoke.Circle(imageWithLines, point, 30, color);
                mainWindow.labels[labelIterator].Text = "x: " + point.X + ", y: " + point.Y;
                labelIterator++;
            }

            CvInvoke.Circle(imageWithLines, inters, 30, new MCvScalar(0, 0, 0, 255));
            if (movementPoints.Any() && mainWindow.movementFlag)
            {
                var dist = handler.PointDistance(inters, movementPoints.Last());
                if (dist > 20 && dist < 300)
                {
                    movementPoints.Add(inters);
                }
            }
            else if (mainWindow.movementFlag)
            {
                movementPoints.Add(inters);

            }
            else
                movementPoints.Clear();

            if (movementPoints.Any())
            {
                Point last = movementPoints[0];
                foreach (var point in movementPoints)
                {
                    //CvInvoke.Circle(ImageFrame, point, 2, new MCvScalar(0, 0, 255, 0));
                    CvInvoke.Line(imageWithLines, last, point, new MCvScalar(0, 0, 255, 0), 2);
                    last = point;
                }
            }

            return imageWithLines;
        }
    }
}
