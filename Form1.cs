﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.ML;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;

using System.IO;
using System.Threading;
using Emgu.CV.Util;

namespace APIS
{
    public partial class Form1 : Form
    {
        private bool mouseIsDown;
        private Capture capture = null;
        private Image<Bgr, Byte> ImageFrame;

        private List<Point> points;
        private Mat img;
        private bool isSavingGroup = false;

        private bool isCapturingCamera = false;

        private int imageListIterator = 0;
        //private List<Point> objectPoints;
        //private Label[] labels;
        public List<Label> labels;

        private List<Point> movementPoints;
        public bool movementFlag = false;
        private Image<Bgr, Byte> ImageFrameBackup;

        private Binarizer binarizer;
        private FileManager file;
        public ObjectHandler objectHandler;
        private Painter painter;
        
        public Form1()
        {
            InitializeComponent();
            binarizer = new Binarizer();
            file = new FileManager();
            objectHandler = new ObjectHandler();
            painter = new Painter(this);

            imageBox1.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;

            points = new List<Point>();
            //objectPoints = new List<Point>();
            labels = new List<Label>();
            movementPoints = new List<Point>();
            labels.Add(label1);
            labels.Add(label2);
            labels.Add(label3);
            labels.Add( label4 );
            Run();
        }

        //private Capture capture;  //takes images from camera as image frames
        private bool captureInProgress = true;

        private void ProcessFrame(object sender, EventArgs arg)
        {
            if (isCapturingCamera)
            {
                img = capture.QueryFrame();
                ImageFrame = img.ToImage<Bgr, Byte>().Flip(FlipType.Vertical).Flip(FlipType.Horizontal);  //line 1
            }

            

            if (ImageFrame != null)
            {
                SetImageBoxes();
            }

            if (file.images.Any())
            {
                ImageFrame = file.images[imageListIterator];
                if (++imageListIterator >= file.images.Count()) imageListIterator = 0;
            }

        }

        private void SetImageBoxes()
        {
            //imageBox1.Image = ImageFrame.Resize(imageBox1.Width, imageBox1.Height, Inter.Linear);  //line 2
            Painter.setImageBox(imageBox1, ImageFrame);
            if (isSavingGroup)
                file.images.Add(ImageFrame);

            var i = binarizer.BinarizeWithOpening(ImageFrame);
            //var i = BinarizeInputImage(ImageFrame);
            Painter.setImageBox(imageBox2, i);
            //imageBox2.Image = i.Resize(imageBox2.Width, imageBox2.Height, Inter.Linear);

            
            var kontury = i.Canny(200, 10);
            Painter.setImageBox(imageBox3, kontury);
            //imageBox3.Image = kontury.Resize(imageBox3.Width, imageBox3.Height, Inter.Linear);

            var imageWithLines = painter.DrawHelperLines(ImageFrame,kontury);
            Painter.setImageBox(imageBox4, imageWithLines);
            //imageBox4.Image = imageWithLines.Resize(imageBox4.Width, imageBox4.Height, Inter.Linear);

            try
            {
                CalculateConvexHull(i);
            }
            catch(Exception e)
            {

            }
            
            //histogramBox1.ClearHistogram();
            //histogramBox1.GenerateHistograms(ImageFrame, 256);
            //histogramBox1.Refresh();
        }

        private void CalculateConvexHull(Image<Gray,byte> img)
        {
            //List<Point> contours = new List<Point>();
            //List<Point> hierarchy = new List<Point>();
            //Mat contours;
            Emgu.CV.Util.VectorOfVectorOfPoint vec = new Emgu.CV.Util.VectorOfVectorOfPoint();
            //CvInvoke.FindContours(img, contours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxSimple);
            //img.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage
            //vec = FindContour(img.ToUMat());

            Image<Gray, byte> imange = img.Clone();
            Image<Bgr, byte> temp = ImageFrame.Clone();

            /*
            Image<Gray, byte> imange2 = new Image<Gray, Byte>(imange.Width, imange.Height);
            for(int i = 0; i < imange.Width; i++)
            {
                for(int j = 0; j < imange.Height; j++)
                {
                    imange2.Data[j+1, i+1, 0] = imange.Data[j, i, 0];
                }
            }*/



            vec = objectHandler.FindContour(imange.ToUMat(),null,RetrType.External);
            
            //CvInvoke.DrawContours(temp, vec, -1, new MCvScalar(255, 0, 0),5);
            double size = 0;
            int idx = 0;
            for (int i = 0; i < vec.Size; i++)
            {
                double size1 = CvInvoke.ContourArea(vec[i]);
                if (size1 > size)
                {
                    size = size1;
                    idx = i;
                }
            }
            //var minarearect = CvInvoke.MinAreaRect(vec[idx]);
            //CvInvoke.DrawContours(temp, minarearect, -1, new MCvScalar(255, 0, 0), 5);
            //CvInvoke.Rectangle(temp, minarearect.MinAreaRect(), new MCvScalar(255, 255, 0));
            //minarearect.
            VectorOfVectorOfPoint vec2 = new Emgu.CV.Util.VectorOfVectorOfPoint(vec.Size);

            //var i = vec[2];
            //var j = i[1];

            
            for (int i = 0; i < vec.Size; i++)
            {
                CvInvoke.ConvexHull(vec[i], vec2[i],false);
            }

            List<Point> vertices = new List<Point>();
            //for(int i = 0; i<vec.Size; i++)

            VectorOfPoint vp = new VectorOfPoint();

            List<PointF> points = new List<PointF>();
            
            for (int i = 0; i < vec.Size; i++)
            {
                CvInvoke.DrawContours(temp, vec, i, new MCvScalar(255, 0, i*10), 2);
                //CvInvoke.DrawContours(temp, vec2, i, new MCvScalar(0, 255, 0), 5);
                
                for (int j = 0; j < vec[i].Size; j++)
                {
                    var aa = vec[i][j];
                    points.Add(aa);
                    
                }
            }

            //points = clusterHullPoints(points);
            PointF[] parr = points.ToArray();

            var conv = CvInvoke.ConvexHull(parr);


            //List<PointF> vect = new List<PointF>;
            var newcontour = Array.ConvertAll(parr, item => Point.Round(item));
            VectorOfPoint contour = new VectorOfPoint(newcontour);
            double ar = CvInvoke.ContourArea(contour);
            var clust = clusterHullPoints(conv, ar);



            //FindConvexityDefects(parr, clust, temp);
            //TestConvexityDefacts(temp);

            
            
            

            foreach (var p in conv)
            {
                temp.Draw(new CircleF(p, 5), new Bgr(100, 100, 255), 2);
            }

            foreach (var p in clust)
            {
                temp.Draw(new CircleF(p, 8), new Bgr(0, 255, 0), 5);
            }
            //Mat m = new Mat()

            
            var newarray = Array.ConvertAll(clust, item => Point.Round(item));
            var newarrayconv = Array.ConvertAll(conv, item => Point.Round(item));
            VectorOfPoint vecc = new VectorOfPoint(newarrayconv);

            
            //CvInvoke.Moments()

            temp.DrawPolyline(newarrayconv, true, new Bgr(0, 150, 255));

            //CvInvoke.ContourArea(points);

            //CvInvoke.ApproxPolyDP()
            Image<Gray, byte> gray = new Image<Gray, byte>(temp.Width, temp.Height);
            gray.FillConvexPoly(newarrayconv, new Gray(255));
            
            Painter.setImageBox(imageBox5, gray);

            //Image<Gray,byte>
            //CvInvoke.InRange(gray.ToUMat(),255,255,)
            int pixhull = CvInvoke.CountNonZero(gray);
            int pixbin = CvInvoke.CountNonZero(img);
            //Painter.setImageBox(imageBox7, img);

            
            double hull_ar = CvInvoke.ContourArea(vecc);
            double solid = ar / hull_ar; //(double)pixhull / pixbin;
            solidity.Text = solid.ToString();
            area.Text = ar.ToString();
            hull_area.Text = hull_ar.ToString();

            double perimeter = CvInvoke.ArcLength(contour,true);
            double roundn = perimeter * perimeter / ar;
            roundness.Text = roundn.ToString();

            double rs = 4 * 3.14 * ar / perimeter;
            Rs.Text = rs.ToString();

            

            var rect = CvInvoke.BoundingRectangle(contour);

            Point[] a = new Point[4]
            {
                new Point(rect.Left, rect.Top),
                new Point(rect.Right, rect.Top),
                new Point(rect.Right, rect.Bottom),
                new Point(rect.Left, rect.Bottom)
            };
            VectorOfPoint brect = new VectorOfPoint(a);
            double bbarea = CvInvoke.ContourArea(brect);
            bbarea = bbarea / ar;
            boxarea.Text = bbarea.ToString();

            gray.Draw(rect, new Gray(255));
            Painter.setImageBox(imageBox7, gray);

            /*
            for(int i = 0; i<clust.Length; i++)
            {
                for(int j = 0; j<clust.Length; j++)
                {

                }
            }*/

            /*open hand 
             * 0.71
             * 7
             * 
             * fist
             * 0.96
             * 3
             * 
             * palm
             * 0.95
             * 5
             * 
             * */
            int hullcount = clust.Length;
            if(solid >= 0.6 && solid <= 0.8 && hullcount >= 6 && hullcount <= 8 && roundn > 45)
            {
                 gestureNumber.Text = "5";
            }
            else if(solid > 0.8 && solid < 1.0 && hullcount <= 4 && hullcount >= 2 && roundn < 35 && roundn > 15) 
            {
                gestureNumber.Text = "10";
            }
            else if (solid > 0.8 && solid < 1.0 && hullcount <= 8 && hullcount >= 6)
            {
                gestureNumber.Text = "7";
            }
            /*else if (roundn > 37 && roundn < 47 && hullcount >= 5 && hullcount <= 10 && solid >= 0.65 && solid <= 0.75)
            {
                gestureNumber.Text = "2";
            }*/
            else
            {
                gestureNumber.Text = "brak";
            }

            //CvInvoke.DrawContours(temp, vec, 0, new MCvScalar(255, 0, 0), 5);
            /*CvInvoke.DrawContours(temp, vec2, idx, new MCvScalar(0, 255, 0), 5);

            for (int j = 0; j < vec2[idx].Size; j++)
            {
                temp.Draw(new Rectangle(vec2[idx][j].X, vec2[idx][j].Y, 5, 5), new Bgr(0, 0, 255), 3);
                vertices.Add(new Point(vec2[idx][j].X, vec2[idx][j].Y));
            }*/

           // for (int i = 0; i < vec2.Size; i++)
            {
                //CvInvoke.DrawContours(temp, vec, i, new MCvScalar(255, 0, i*10), 5);
                //CvInvoke.DrawContours(temp, vec2, i, new MCvScalar(0, 255, 0), 5);
                /*
                for (int j = 0; j < vec2[i].Size; j++)
                {
                    temp.Draw(new Rectangle(vec2[i][j].X, vec2[i][j].Y, 5, 5), new Bgr(0, 0, 255), 3);
                    vertices.Add(new Point(vec2[i][j].X, vec2[i][j].Y));
                }*/
                //temp.Draw(new Rectangle((int)vec2[i].X, (int)vec2[i].Y, 0, 0), new Bgr(0, 0, 255), 3);
            }
            
            Painter.setImageBox(imageBox6, temp);
            /*
            
            //CvInvoke.DrawContours(temp, vec2, -1, new MCvScalar(0, 255, 0), 5);
            for(int i = 0; i<vec2.Size; i++)
            {
                CvInvoke.DrawContours(temp, vec, i, new MCvScalar(255, 0, 0), 5);
                CvInvoke.DrawContours(temp, vec2, i, new MCvScalar(0, 255, 0), 5);
                for(int j = 0; j<vec2[i].Size; j++)
                {
                    temp.Draw(new Rectangle(vec2[i][j].X, vec2[i][j].Y, 5, 5), new Bgr(0, 0, 255), 3);
                }
                //temp.Draw(new Rectangle((int)vec2[i].X, (int)vec2[i].Y, 0, 0), new Bgr(0, 0, 255), 3);
            }*/
            //img.Fi
            //CvInvoke.ConvexHull(vec.ToArrayOfArray());
            //imageBox6.Image = temp;
            //imageBox6.Image = temp.Resize(imageBox6.Width, imageBox6.Height, Inter.Linear);
        }

        public void TestConvexityDefacts(Image<Bgr,byte> image)
        {
            //Image<Bgr, Byte> image = new Image<Bgr, byte>(300, 300);
            Point[] polyline = new Point[] {
            new Point(10, 10),
            new Point(10, 250),
            new Point(100, 100),
            new Point(250, 250),
            new Point(250, 10)};
            using (VectorOfPoint vp = new VectorOfPoint(polyline))
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint(vp))
            using (VectorOfInt convexHull = new VectorOfInt())
            using (Mat convexityDefect = new Mat())
            {
                //Draw the contour in white thick line
                CvInvoke.DrawContours(image, contours, -1, new MCvScalar(255, 255, 255), 3);
                CvInvoke.ConvexHull(vp, convexHull);
                CvInvoke.ConvexityDefects(vp, convexHull, convexityDefect);

                //convexity defect is a four channel mat, when k rows and 1 cols, where k = the number of convexity defects. 
                if (!convexityDefect.IsEmpty)
                {
                    //Data from Mat are not directly readable so we convert it to Matrix<>
                    Matrix<int> m = new Matrix<int>(convexityDefect.Rows, convexityDefect.Cols,
                       convexityDefect.NumberOfChannels);
                    convexityDefect.CopyTo(m);

                    for (int i = 0; i < m.Rows; i++)
                    {
                        int startIdx = m.Data[i, 0];
                        int endIdx = m.Data[i, 1];
                        int depthIdx = m.Data[i, 2];
                        Point startPoint = polyline[startIdx];
                        Point endPoint = polyline[endIdx];
                        Point depthPoint = polyline[depthIdx];

                        //draw  a line connecting the convexity defect start point and end point in thin red line
                        //CvInvoke.Line(image, startPoint, endPoint, new MCvScalar(255, 0, 0),3);
                        CvInvoke.Line(image, startPoint, depthPoint, new MCvScalar(0, 255, 0), 3);
                        CvInvoke.Line(image, endPoint, depthPoint, new MCvScalar(0, 0, 255), 3);
                    }
                }

                //Emgu.CV.UI.ImageViewer.Show(image);
            }
        }


        public void FindConvexityDefects(PointF[] parr, PointF[] hull, Image<Bgr, byte> image)
        {
            var pcontours = Array.ConvertAll(parr, item => Point.Round(item));
            VectorOfPoint contour = new VectorOfPoint(pcontours);

            VectorOfInt convexHull = new VectorOfInt();
            CvInvoke.ConvexHull(contour, convexHull);

            
            int[] cl = new int[hull.Length];
            for(int i = 0; i < cl.Length; i++)
            {
                cl[i] = Array.IndexOf(parr, Array.Find(parr, x => (x.X == hull[i].X && x.Y == hull[i].Y)));
            }
            VectorOfInt clHull = new VectorOfInt(cl);

            var pclustered = Array.ConvertAll(hull, item => Point.Round(item));
            VectorOfPoint clusteredhull = new VectorOfPoint(pclustered);
            Mat convexityDefect = new Mat();


            //CvInvoke.ConvexHull(clusteredhull, convexHull);

            CvInvoke.ConvexityDefects(contour, convexHull, convexityDefect);
            
            
            if (!convexityDefect.IsEmpty)
            {
                //Data from Mat are not directly readable so we convert it to Matrix<>
                Matrix<int> m = new Matrix<int>(convexityDefect.Rows, convexityDefect.Cols,
                   convexityDefect.NumberOfChannels);
                convexityDefect.CopyTo(m);
                

                for (int i = 0; i < m.Rows; i++)
                {
                    int startIdx = m.Data[i, 0];
                    int endIdx = m.Data[i, 1];

                    int depthIdx = m.Data[i, 2];
                    
                    
                    Point startPoint = Point.Round(parr[startIdx]);
                    Point endPoint = Point.Round(parr[endIdx]);
                    Point depthPoint = Point.Round(parr[depthIdx]);

                    //draw  a line connecting the convexity defect start point and end point in thin red line
                    //CvInvoke.Line(image, startPoint, endPoint, new MCvScalar(0, 0, 255), 5);
                    //CvInvoke.Line(image, startPoint2, endPoint2, new MCvScalar(0, 0, 255), 5);
                    CvInvoke.Circle(image, startPoint, 5, new MCvScalar(0, 255, 0), 2);
                    CvInvoke.Circle(image, endPoint, 5, new MCvScalar(0, 255,0), 2);
                    CvInvoke.Circle(image, depthPoint, 5, new MCvScalar(0, 255, 0), 2);


                }

            }
            else
            {
                //CvInvoke.Line(image, new Point(100,100),new Point(300,300), new MCvScalar(0,0,255),5);
            }

        }

        public PointF[] clusterHullPoints(PointF[] hull, double area)
        {
            // =============== CvInvoke.ConvexityDefects ~!!!!!!!!!!!!!!!!!! ========================
            double distanceThreshold = 60;

            List<PointF> newHull = new List<PointF>();

            List<List<PointF>> groups = new List<List<PointF>>();

            for (int i = 0; i < hull.Length; i++)
            {
                List<PointF> subGroup = new List<PointF>();
                subGroup.Add(new PointF(hull[i].X,hull[i].Y));
                for (int j = 0; j < hull.Length; j++)
                {
                    if (i != j)
                    {
                        double dist = GetDistance(hull[i], hull[j]);
                        if (dist < distanceThreshold)
                        {
                            subGroup.Add(new PointF(hull[j].X, hull[j].Y));
                        }
                    }

                }
                
                bool shouldIAdd = true;
                foreach (var group in groups)
                {
                    bool flag = false;
                    foreach (var point in group)
                    {
                        if (point.X == hull[i].X && point.Y == hull[i].Y)
                        {
                            flag = true;
                            break;
                        }
                    }

                    if (flag)
                    {
                        foreach(var point in subGroup)
                        {
                            bool flag2 = true;
                            foreach(var p in group)
                            {
                                if((point.X == p.X && point.Y == p.Y) 
                                    || (point.X == p.Y && point.Y == p.X))
                                {
                                    flag2 = false;
                                }
                            }
                            if (flag2)
                                group.Add(point);
                        }
                        //group.AddRange(subGroup);
                        shouldIAdd = false;
                    }
                }

                if(shouldIAdd)
                    groups.Add(subGroup);

            }

            


            float maxPointNumber = 0;
            float minPointNumber = 1500;
            Dictionary<int, int> groupCount = new Dictionary<int,int>();
            
            int iterator = 0;
            foreach(var group in groups)
            {
                float xmean = 0;
                float ymean = 0;
                int pointNumber = 0;
                foreach (var point in group)
                {
                    xmean = xmean + point.X;
                    ymean = ymean + point.Y;
                    pointNumber++;
                }
                xmean = xmean / pointNumber;
                ymean = ymean / pointNumber;
                //if(pointNumber >= 5)
                newHull.Add(new PointF(xmean, ymean));
                if (pointNumber > maxPointNumber)
                    maxPointNumber = pointNumber;
                if (pointNumber < minPointNumber)
                    minPointNumber = pointNumber;

                groupCount[iterator] = pointNumber;
                iterator++;
            }

            float diff = maxPointNumber - minPointNumber;
            diff = maxPointNumber - (2*diff/3);
            List<PointF> nHull = new List<PointF>();
            int groupiter = 0;
            for(int i = 0; i < groupCount.Count; i++)
            {
                //if (groupCount[i] > diff)
                //if(groupiter < 5)
                    nHull.Add(newHull[i]);
                groupiter++;
            }

            ClusterNumber.Text = nHull.Count.ToString();
            /*
            List<PointF> nHull = new List<PointF>();
            foreach (var point in newHull)
            {
                PointF min = hull[0];
                for(int i = 1; i < hull.Length; i++)
                {
                    PointF p = hull[i];
                    if(Math.Abs(p.X - point.X) < Math.Abs(min.X - point.X)
                        && Math.Abs(p.Y - point.Y) < Math.Abs(min.X - point.X))
                    {
                        min = p;
                    }
                }
                nHull.Add(min);
            }
            */
            return nHull.ToArray();
        }

        public double GetDistance(PointF point1, PointF point2)
        {
            double a = (double)(point2.X - point1.X);
            double b = (double)(point2.Y - point1.Y);

            return Math.Sqrt(a * a + b * b);
        }
        

        private void Run()
        {
            try
            {
                capture = new Capture();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
                return;
            }
            Application.Idle += ProcessFrame;
            
        }
        private void Form1_Load(object sender, EventArgs e)
        {

            /*
            if (capture != null)
            {
                if (captureInProgress)
                {  //if camera is getting frames then stop the capture and set button Text
                   // "Start" for resuming capture
                    //btnStart.Text = "Start!"; //
                    Application.Idle -= ProcessFrame;
                }
                else
                {
                    //if camera is NOT getting frames then start the capture and set button
                    // Text to "Stop" for pausing capture
                    //btnStart.Text = "Stop";
                    Application.Idle += ProcessFrame;
                }
                //captureInProgress = !captureInProgress;
            }*/
        }

        private void SaveImage_Click(object sender, EventArgs e)
        {
            /*
            Image<Bgr, Byte> img = ImageFrame;
            //ImageFrame.Save(@"E:\capture");
            //Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Image Files (*.tif; *.dcm; *.jpg; *.jpeg; *.bmp)|*.tif; *.dcm; *.jpg; *.jpeg; *.bmp";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    // Code to write the stream goes here.
                    img.Save(saveFileDialog1.FileName);
                    //myStream.Close();
                }
            }*/

            file.SaveImage(ImageFrame);
        }

        private void startGroupSave_Click(object sender, EventArgs e)
        {
            isSavingGroup = true;
        }

        private void stopGroupSave_Click(object sender, EventArgs e)
        {
            isSavingGroup = false;

            file.SaveImages();
            /*
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Image Files (*.tif; *.dcm; *.jpg; *.jpeg; *.bmp)|*.tif; *.dcm; *.jpg; *.jpeg; *.bmp";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                int i = 1;
                foreach (var img in file.images)
                {
                    img.Save(saveFileDialog1.FileName.Split('.').First() + i.ToString() + '.' + saveFileDialog1.FileName.Split('.').Last());
                    ++i;
                }

            }*/

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void captureCamera_Click(object sender, EventArgs e)
        {
            if(isCapturingCamera)
            {
                isCapturingCamera = false;
                captureCamera.Name = "Capture";
            }
            else
            {
                isCapturingCamera = true;
                captureCamera.Name = "Stop Capture";
            }
            
        }

        private void imageBox1_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            Point coords = me.Location;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void trackObjects_Click(object sender, EventArgs e)
        {
            if (movementFlag == false)
            {
                movementFlag = true;
                trackObjects.Text = "Untrack";
            }
            else
            {
                movementFlag = false;
                trackObjects.Text = "Track";
            }
        }

        private void imageBox3_Click(object sender, EventArgs e)
        {
        }

        private void histogramBox1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ImageFrame.ROI = new Rectangle(imageBox1.Location.X+200, imageBox1.Location.Y+200, imageBox1.Width/3, imageBox1.Height/3);
            var i = ImageFrame.Clone().Resize(imageBox5.Width, imageBox5.Height, Inter.Linear);
            imageBox5.Image = i;
            
        }

        private void imageBox5_Click(object sender, EventArgs e)
        {
        }

        private void imageBox4_Click(object sender, EventArgs e)
        {
        }

        private void saveImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            file.SaveImage(ImageFrame);
        }

        private void startSaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isSavingGroup = true;
        }

        private void stopSaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isSavingGroup = false;
            file.SaveImages();
        }

        private void loadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loadSingleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isCapturingCamera = false;
            file.LoadImage();
            ImageFrame = file.image.Clone();
        }

        private void loadGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isCapturingCamera = false;
            file.LoadImages();
        }

        private void imageBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                mouseIsDown = true;
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ImageFrame = ImageFrameBackup.Clone();
                imageBox1.Image = ImageFrame;
            }
        }

        private void imageBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown && ImageFrame != null)
            {
                Point point = imageBox1.PointToClient(Cursor.Position);
                ImageFrame.Draw(new Rectangle(point.X * ImageFrame.Width / imageBox1.Width, point.Y * ImageFrame.Height / imageBox1.Height, 0,0), new Bgr(0, 0, 255), 3);
                points.Add(point);
            }
        }

        private void imageBox1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        public void DrawPoint(int x, int y, Color color)
        {
            Graphics g = imageBox1.CreateGraphics();
            Pen pen = new Pen(color);
            g.DrawRectangle(pen, x, y, 3, 3);
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        }

        private void imageBox6_Click(object sender, EventArgs e)
        {
        }

        private void imageBox7_Click(object sender, EventArgs e)
        {

        }

        private void imageBox8_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click_1(object sender, EventArgs e)
        {

        }

        private void label5_Click_2(object sender, EventArgs e)
        {

        }
    }
}
